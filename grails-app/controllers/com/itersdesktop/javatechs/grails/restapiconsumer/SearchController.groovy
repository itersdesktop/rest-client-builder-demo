package com.itersdesktop.javatechs.grails.restapiconsumer

class SearchController {
    def searchService

    def index() {
        def result = searchService.retrieveBioModelsAllCuratedModels()
        def map = ["message": "There is an error when retrieving all curated models from BioModels via RESTful API"]
        if (null) {
            render map["message"]
        } else {
            render result
        }
    }

    def curated() {
        def results = searchService.retrieveBioModelsAllCuratedModels()
        def map = ["message": "There is an error when retrieving all curated models from BioModels via RESTful API"]
        if (null) {
            render map["message"]
        } else {
            def models = results.models
            [models: models, title: "All curated models fetched from BioModels"]
        }
    }
}
