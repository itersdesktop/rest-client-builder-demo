<%--
  Created by IntelliJ IDEA.
  User: tnguyen
  Date: 10/05/2020
  Time: 14:16
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>${title}</title>
</head>

<body>
<h2>Here are the first ten curated models in
    <a href="https://www.ebi.ac.uk/biomodels/" target="_blank">BioModels</a></h2>
    <g:render template="/templates/search/modelList" model="['models': models]"/>
</body>
</html>
