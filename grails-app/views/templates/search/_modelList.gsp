<%--
  Created by IntelliJ IDEA.
  User: tnguyen
  Date: 10/05/2020
  Time: 14:22
--%>

<table>
    <tr>
        <th>Model Id</th>
        <th>Submitter</th>
        <th>Format</th>
        <th>Name</th>
        <th>Submission Date</th>
        <th>Last Modified</th>
        <th>Link</th>
    </tr>
    <g:each in="${models}" var="model">
        <tr>
            <td>${model.id}</td>
            <td>${model.submitter}</td>
            <td>${model.format}</td>
            <td>${model.name}</td>
            <td>${model.submissionDate}</td>
            <td>${model.lastModified}</td>
            <td><a href="${model.url}" target="_blank">${model.id}</a></td>
        </tr>
    </g:each>
</table>
