package com.itersdesktop.javatechs.grails.restapiconsumer

import grails.plugins.rest.client.RestBuilder
import grails.transaction.Transactional
import org.codehaus.groovy.grails.web.json.JSONElement

@Transactional
class SearchService {
    JSONElement retrieveBioModelsAllCuratedModels() {
        final String BM_SEARCH_URL = "https://www.ebi.ac.uk/biomodels/search?domain=biomodels"
        String queryURL = """\
${BM_SEARCH_URL}&query=*:* AND curationstatus:\"Manually curated\" AND NOT isprivate:true&format=json"""
        println queryURL
        RestBuilder rest = new RestBuilder(connectTimeout: 10000, readTimeout: 100000, proxy: null)
        rest.restTemplate.messageConverters.removeAll {
            it.class.name == 'org.springframework.http.converter.json.GsonHttpMessageConverter'
        }
        def response = rest.get(queryURL) {
            accept("application/json")
            accept("application/xml")
            contentType("application/json;charset=UTF-8")
        }
        if (response.status == 200) {
            return response.json
        }
        return null
    }
}
